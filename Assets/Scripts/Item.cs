﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

     public int id;
     public string itemName;
     public float weight;
     public float price;
     public ItemType group = ItemType.none;

     public GameObject itemPrefab;

     public void MakeKinematic(){
          Invoke ("MakeKinematicReal", 2);
     }

     void MakeKinematicReal(){
          GetComponent<Rigidbody> ().isKinematic = true;
          GetComponent<BoxCollider> ().isTrigger = true;
     }

     void OnTriggerStay (Collider other){
          if (other.tag.Equals ("Player1")) {               
               if (Input.GetButton ("PickBottomP1") || Input.GetButton ("PickTopP1")) {
                    PickItem (other.gameObject);
               }
          }

          if (other.tag.Equals ("Player2")) {
               if (Input.GetButton ("PickBottomP2") || Input.GetButton ("PickTopP2")) {
                    PickItem (other.gameObject);
               }
          }
     }

     void PickItem(GameObject player){
          if (player.GetComponent<PlayerControl> ().currWeight < player.GetComponent<PlayerControl> ().maxWeight) {               
               player.GetComponent<PlayerControl> ().currWeight += weight;
               if (player.GetComponent<PlayerControl> ().currWeight > player.GetComponent<PlayerControl> ().maxWeight) player.GetComponent<PlayerControl> ().currWeight = player.GetComponent<PlayerControl> ().maxWeight;
               player.GetComponent<PlayerControl> ().cash += price;
               player.GetComponent<PlayerControl> ().items.Add (gameObject);
               gameObject.SetActive (false);
          }
     }
}
