﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSelectionController : MonoBehaviour {

     public PlayerControl player1;
     public PlayerControl player2;

     public Image imageP1;
     public Image imageP2;

     public Text descriptionP1;
     public Text descriptionP2;

     public GameObject pl1ReadyPnl;
     public GameObject pl2ReadyPnl;

     public GameObject StartCountdownPanel;

     public Sprite spriteA;
     public Sprite spriteB;
     public Sprite spriteC;
     public Sprite spriteD;

     bool pl1Ready = false;
     bool pl2Ready = false;


	// Use this for initialization
	void Start () {
          PlayerImageType p1Image = GetRandomEnum<PlayerImageType>();
          PlayerImageType p2Image = GetRandomEnum<PlayerImageType>();
          while(p2Image == p1Image) p2Image = GetRandomEnum<PlayerImageType>();

          PlayerHistoryType p1History = GetRandomEnum<PlayerHistoryType>();
          PlayerHistoryType p2History = GetRandomEnum<PlayerHistoryType>();
          while(p2History == p1History) p2History = GetRandomEnum<PlayerHistoryType>();

          player1.playerHistory = p1History;
          player2.playerHistory = p2History;

          SetImage (imageP1, p1Image);
          SetImage (imageP2, p2Image);

          SetDescriptionText (descriptionP1, p1History);
          SetDescriptionText (descriptionP2, p2History);

          player1.SetAvatar (p1Image);
          player2.SetAvatar (p1Image);
	}
	
	// Update is called once per frame
	void Update () {
          if (GameController.Instance.currState == GameStateType.GameStart) {
               if (pl1Ready && pl2Ready) {
                    // Start game !!
                    GameController.Instance.currState = GameStateType.Waiting;
                    StartCountdownPanel.SetActive (true);
                    gameObject.SetActive (false);
               }
          }

          if (Input.GetButton ("PickBottomP1")) {
               pl1Ready = true;
               pl1ReadyPnl.SetActive (true);
          }
          if (Input.GetButton ("PickBottomP2")) {
               pl2Ready = true;
               pl2ReadyPnl.SetActive (true);
          }
	}

     static T GetRandomEnum<T>()
     {
          System.Array A = System.Enum.GetValues(typeof(T));
          T V = (T)A.GetValue(UnityEngine.Random.Range(0,A.Length));
          return V;
     }

     public void SetDescriptionText (Text descriptionTxt, PlayerHistoryType pHistory){
          if (pHistory == PlayerHistoryType.a)        descriptionTxt.text = "Acaba de independizarse a un piso sin amueblar y aún no sabe cual es el mágico recorrido que hace la ropa del cesto a su armario pero tiene ganas de averiguarlo, también siente curiosidad por cómo la nevera se llena de comida semanalmente.";
          else if (pHistory == PlayerHistoryType.b)   descriptionTxt.text = "Ha terminado la carrera de informática y quiere incorporarse al mercado laboral como un flamante becario...bueno, en cuanto termine la partida de dragones y mazmorras que está jugando.";
          else if (pHistory == PlayerHistoryType.c)   descriptionTxt.text = "Quiere ser el próximo Skrillex sin que su peinado de penita, DJ por vocación está preparándose para ser el próximo pelotazo en las fiestas del verano. Todos sabemos que acabará trabajando en bodas.";
          else if (pHistory == PlayerHistoryType.d)   descriptionTxt.text = "No entiende porqué la gente no sigue sus podcast de 8 horas sobre juego de tronos, por suerte es muy “popular” en el foro donde escribe análisis sobre series y juegos actuales. Le encanta el plano holandés y piensa que el gran lebowski está sobrevalorada.";
     }

     public void SetImage(Image imageDest, PlayerImageType pImage){
          if (pImage == PlayerImageType.a)         imageDest.sprite = spriteA;
          else if (pImage == PlayerImageType.b)    imageDest.sprite = spriteB;
          else if (pImage == PlayerImageType.c)    imageDest.sprite = spriteC;
          else if (pImage == PlayerImageType.d)    imageDest.sprite = spriteD;
     }
}     