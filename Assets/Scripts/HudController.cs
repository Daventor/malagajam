﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class HudController : MonoBehaviour {

     public Text timeLbl;
     public Text timeMiliLbl;

     public RectTransform p1Pointer;
     public RectTransform p2Pointer;

     public Text itemsP1;
     public Text itemsP2;

     public GameObject gameOverBlock;


     public Text p1TotalItems;
     public Text p1TotalPrice;
     public Text p1TotalWeight;
     public Text p1TotalPoints;

     public Text p2TotalItems;
     public Text p2TotalPrice;
     public Text p2TotalWeight;
     public Text p2TotalPoints;

     public Text winnerLbl;
     public Text restartReadyLbl;


     PlayerControl player1;
     PlayerControl player2;

     Cashbox cashBoxP1;
     Cashbox cashBoxP2;

     bool canRestart = false;
	
     void Start() {
          player1 = GameObject.FindGameObjectWithTag ("Player1").GetComponent<PlayerControl>();
          player2 = GameObject.FindGameObjectWithTag ("Player2").GetComponent<PlayerControl>();

          cashBoxP1 = GameObject.FindGameObjectWithTag ("CashBoxP1").GetComponent<Cashbox>();
          cashBoxP2 = GameObject.FindGameObjectWithTag ("CashBoxP2").GetComponent<Cashbox>();
     }
          	
	void Update () {          
          float currTime = GameController.Instance.gameTime;
          float limitTime = GameController.Instance.gameLimit;

          float timeleft = limitTime - currTime;
          if (timeleft < 0) timeleft = 0;

          // Convert to human time
          TimeSpan humanTimeLeft = TimeSpan.FromSeconds(timeleft);

          timeLbl.text = humanTimeLeft.Minutes.ToString() + ":" + ((humanTimeLeft.Seconds < 10)?"0" + humanTimeLeft.Seconds.ToString():humanTimeLeft.Seconds.ToString());
          timeMiliLbl.text = ":" + ((humanTimeLeft.Milliseconds < 100)?"0" + Mathf.RoundToInt(humanTimeLeft.Milliseconds/10).ToString():Mathf.RoundToInt(humanTimeLeft.Milliseconds/10).ToString());

          if (timeleft < 31) {
               timeLbl.color = timeMiliLbl.color = Color.red;
          } else {
               timeLbl.color = timeMiliLbl.color = Color.white;
          }

          //----------------------------------------------------

          // Player weight perc
          p1Pointer.rotation = Quaternion.Euler (new Vector3 (0, 0, 180 - (180 * (player1.currWeight/player1.maxWeight))));
          p2Pointer.rotation = Quaternion.Euler (new Vector3 (0, 0, 180 - (180 * (player2.currWeight/player2.maxWeight))));

          itemsP1.text = ((cashBoxP1.items.Count < 10)?"0" + cashBoxP1.items.Count:cashBoxP1.items.Count.ToString());
          itemsP2.text = ((cashBoxP2.items.Count < 10)?"0" + cashBoxP2.items.Count:cashBoxP2.items.Count.ToString());

          if (canRestart && Input.GetButton ("PickBottomP1")) {
               SceneManager.LoadScene ("Sandbox");
          }
	}          

     public void EnableGameOver(){
          float p1TotalPriceNb = 0;
          float p1TotalWeightNb = 0;
          int p1TotalPointsNb = 0;
          foreach (GameObject item in cashBoxP1.items) {
               p1TotalPriceNb += item.GetComponent<Item> ().price;
               p1TotalWeightNb += item.GetComponent<Item> ().weight;

               p1TotalPointsNb += GetPointFromItem (player1.playerHistory, item.GetComponent<Item> ());
          }

          p1TotalItems.text = cashBoxP1.items.Count.ToString ();
          p1TotalPrice.text = p1TotalPriceNb.ToString () + " €";
          p1TotalWeight.text = p1TotalWeightNb.ToString () + " Kg";
          p1TotalPoints.text = p1TotalPointsNb.ToString ();

          float p2TotalPriceNb = 0;
          float p2TotalWeightNb = 0;
          int p2TotalPointsNb = 0;
          foreach (GameObject item in cashBoxP2.items) {
               p2TotalPriceNb += item.GetComponent<Item> ().price;
               p2TotalWeightNb += item.GetComponent<Item> ().weight;

               p2TotalPointsNb += GetPointFromItem (player2.playerHistory, item.GetComponent<Item> ());
          }

          p2TotalItems.text = cashBoxP2.items.Count.ToString ();
          p2TotalPrice.text = p2TotalPriceNb.ToString () + " €";
          p2TotalWeight.text = p2TotalWeightNb.ToString () + " Kg";
          p2TotalPoints.text = p2TotalPointsNb.ToString ();

          if (p1TotalPointsNb > p2TotalPointsNb)           winnerLbl.text = "Jugador 1";
          else if (p1TotalPointsNb < p2TotalPointsNb)      winnerLbl.text = "Jugador 2";
          else {
               if (p1TotalPriceNb > p2TotalPriceNb)        winnerLbl.text = "Jugador 1";
               else                                        winnerLbl.text = "Jugador 2";
          }

          gameOverBlock.SetActive (true);

          Invoke ("EnableRestart", 5);
     }

     int GetPointFromItem(PlayerHistoryType history, Item item){
          int point = 0;
          if (history == PlayerHistoryType.a) {
               if (item.group == ItemType.appliances)       point = 4;
               else if (item.group == ItemType.computing)   point = 3;
               else if (item.group == ItemType.sound)       point = 1;
               else if (item.group == ItemType.cultural)    point = 2;
          }else if (history == PlayerHistoryType.b) {
               if (item.group == ItemType.appliances)       point = 1;
               else if (item.group == ItemType.computing)   point = 4;
               else if (item.group == ItemType.sound)       point = 2;
               else if (item.group == ItemType.cultural)    point = 3;
          }else if (history == PlayerHistoryType.c) {
               if (item.group == ItemType.appliances)       point = 3;
               else if (item.group == ItemType.computing)   point = 2;
               else if (item.group == ItemType.sound)       point = 4;
               else if (item.group == ItemType.cultural)    point = 1;
          }else if (history == PlayerHistoryType.d) {
               if (item.group == ItemType.appliances)       point = 2;
               else if (item.group == ItemType.computing)   point = 1;
               else if (item.group == ItemType.sound)       point = 3;
               else if (item.group == ItemType.cultural)    point = 4;
          }
               
          return point;
     }

     void EnableRestart(){
          restartReadyLbl.gameObject.SetActive (true);
          canRestart = true;
     }
}