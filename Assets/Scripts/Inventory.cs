﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

     public List<GameObject> items = new List<GameObject> ();

     public List<GameObject> itemsNone = new List<GameObject> ();
     public List<GameObject> itemsComputing = new List<GameObject> ();
     public List<GameObject> itemsCultural = new List<GameObject> ();
     public List<GameObject> itemsAppliances = new List<GameObject> ();
     public List<GameObject> itemsSound = new List<GameObject> ();

     private static Inventory instance = null;

     void Awake() {
          if (instance != null && instance != this) {
               Destroy(this.gameObject);
               return;
          } else {               
               instance = this;
          }
     }

     public static Inventory Instance {
          get { return instance; }
     }

	// Use this for initialization
	void Start () {
	     // Generate groups
          foreach(GameObject item in items){
               if (item.GetComponent<Item> ().group == ItemType.computing)           itemsComputing.Add (item);
               else if (item.GetComponent<Item> ().group == ItemType.cultural)       itemsCultural.Add (item);
               else if (item.GetComponent<Item> ().group == ItemType.appliances)     itemsAppliances.Add (item);
               else if (item.GetComponent<Item> ().group == ItemType.sound)          itemsSound.Add (item);
               else itemsNone.Add (item);
          }
	}
	
     public GameObject GetItem(ItemType itemGroup, int perc){          
          GameObject itemSelected;

          if (Random.Range (0, 100) < perc) {
               List<GameObject> itemsGroup;
               if (itemGroup == ItemType.computing)         itemsGroup = itemsComputing;
               else if (itemGroup == ItemType.appliances)   itemsGroup = itemsAppliances;
               else if (itemGroup == ItemType.cultural)     itemsGroup = itemsCultural;
               else if (itemGroup == ItemType.sound)        itemsGroup = itemsSound;
               else itemsGroup = itemsNone;

               itemSelected = itemsGroup[Random.Range (0, itemsGroup.Count)];
          } else {
               itemSelected = GetItem ();
          }
               
          return itemSelected;
     }

     public GameObject GetItem(){
          return items [Random.Range (0, items.Count)];
     }
}