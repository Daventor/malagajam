﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

     public float gameTime = 0;
     public float gameLimit = 180;
     public GameStateType currState = GameStateType.InGame;

     private static GameController instance = null;

     void Awake() {
          if (instance != null && instance != this) {
               Destroy(this.gameObject);
               return;
          } else {               
               instance = this;
          }
     }

     public static GameController Instance {
          get { return instance; }
     }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
          if (currState == GameStateType.InGame) gameTime += Time.deltaTime;

          if (gameTime >= gameLimit && currState != GameStateType.GameEnd) {
               currState = GameStateType.GameEnd;

               GameObject.FindGameObjectWithTag ("Hud").GetComponent<HudController> ().EnableGameOver ();
          }
	}
}