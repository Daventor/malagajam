﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : MonoBehaviour {

     public float maxWeight = 100;
     public float currWeight = 0;
     public float speed = 4.5f;
     public float cash = 0;
     public List<GameObject> items = new List<GameObject> ();

     public bool player1 = true;
     public bool canMove = true;

     public GameObject PlayerCam;
     public GameObject model;

     public PlayerImageType playerAvatar;
     public PlayerHistoryType playerHistory;

     Rigidbody rb;

     float horizontal;
     float vertical;
     bool speedBtn;
     bool throwBtn;


	// Use this for initialization
	void Start () {
          rb = GetComponent<Rigidbody> ();

          int power = Random.Range (3, 8);
          float agility = 10 - power;

          maxWeight = power * 10;
          speed = speed * (1 + (agility / 10));

	}
	
	// Update is called once per frame
	void Update () {
          HandleInputs ();
          if (rb.velocity != Vector3.zero)
               model.transform.rotation = Quaternion.LookRotation (new Vector3(rb.velocity.x, 0, rb.velocity.z));

          if (throwBtn) {
               DropItem ();
          }
     }

     void FixedUpdate(){
          HandleMovement ();

     }

     void HandleInputs (){
          if (player1) {
               horizontal = Input.GetAxis ("HorizontalP1");
               vertical = Input.GetAxis ("VerticalP1");

               speedBtn= Input.GetButton ("SpeedP1");
               throwBtn = Input.GetButtonUp ("ThrowP1");
          } else {
               horizontal = Input.GetAxis ("HorizontalP2");
               vertical = Input.GetAxis ("VerticalP2");

               speedBtn = Input.GetButton ("SpeedP2");
               throwBtn = Input.GetButtonUp ("ThrowP2");
          }

     }

     void HandleMovement(){
          if (GameController.Instance.currState == GameStateType.InGame && canMove && (horizontal != 0 || vertical != 0)) {
               Vector3 storeDir = PlayerCam.transform.right;          
               storeDir.y = 0;
               Vector3 dirForward = storeDir * horizontal;
               Vector3 sides = PlayerCam.transform.forward;
               sides.y = 0;
               Vector3 dirSides = sides * vertical;

               Vector3 targetVelocity = (dirForward + dirSides).normalized;
               float finalSpeed = speed;

               // 50% weight can not run
               if(speedBtn && currWeight <= (maxWeight/2)){ 
                    float speedInc = speed;
                    if (currWeight > (maxWeight * 0.4f))  speedInc = speedInc * 0.2f;
                    else if (currWeight > (maxWeight * 0.3f))  speedInc = speedInc * 0.4f;
                    else if (currWeight > (maxWeight * 0.2f))  speedInc = speedInc * 0.6f;
                    else if (currWeight > (maxWeight * 0.1f))  speedInc = speedInc * 0.8f;

                    finalSpeed += speedInc;
               }

               if (currWeight == maxWeight)                 finalSpeed = finalSpeed * 0.1f;
               else if (currWeight > (maxWeight * 0.9f))    finalSpeed = finalSpeed * 0.2f;
               else if (currWeight > (maxWeight * 0.8f))    finalSpeed = finalSpeed * 0.4f;
               else if (currWeight > (maxWeight * 0.7f))    finalSpeed = finalSpeed * 0.6f;
               else if (currWeight > (maxWeight * 0.6f))    finalSpeed = finalSpeed * 0.8f;

               targetVelocity *= finalSpeed;

               float maxVelocityChange = 1.0f;
               Vector3 velocity = rb.velocity;
               Vector3 velocityChange = (targetVelocity - velocity);
               velocityChange.x = Mathf.Clamp (velocityChange.x, -maxVelocityChange, maxVelocityChange);
               velocityChange.z = Mathf.Clamp (velocityChange.z, -maxVelocityChange, maxVelocityChange);
               velocityChange.y = 0;

               rb.AddForce (velocityChange, ForceMode.VelocityChange);
          }
     }

     void DropItem(){
          if (items.Count > 0) {
               GameObject dropItem = items [items.Count - 1];
               dropItem.SetActive (true);
               items.Remove (dropItem);

               currWeight -= dropItem.GetComponent<Item> ().weight;
               if (currWeight < 0) currWeight = 0;
               cash -= dropItem.GetComponent<Item> ().price;
               if (cash < 0) cash = 0;

               dropItem.transform.position = transform.position + transform.up;
               dropItem.GetComponent<BoxCollider> ().isTrigger = false;
               dropItem.GetComponent<Rigidbody> ().isKinematic = false;
               dropItem.GetComponent<Rigidbody> ().AddForce (new Vector3 (Random.Range(-10, 10), 2.5f, Random.Range(-10, 10)), ForceMode.Impulse);
               dropItem.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)), ForceMode.Impulse);
               dropItem.GetComponent<Item>().MakeKinematic ();
          }
     }

     public void EnableMove(float time){
          Invoke ("EnableMoveReal", time);
     }

     void EnableMoveReal(){
          canMove = true;
     }

     public void SetAvatar(PlayerImageType avatar){
          playerAvatar = avatar;


     }
}