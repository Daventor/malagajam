﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rack : MonoBehaviour {

     public GameObject topItem;
     public GameObject bottomItem;

     public ItemType topGroup = ItemType.none;
     public int topPerc = 0;

     public ItemType bottomGroup = ItemType.none;
     public int bottomPerc = 0;

     public GameObject topPoint;
     public GameObject bottomPoint;

     bool topPicked = false;
     bool bottomPicked = false;

	// Use this for initialization
	void Start () {
          if (topItem == null)     GenerateTopItem ();
          if (bottomItem == null)  GenerateBottomItem ();
	}

     void OnTriggerStay (Collider other)
     {
          if (other.tag.Equals ("Player1")) {               
               if (Input.GetButton ("PickBottomP1")) {
                    PickItem (other.gameObject, 0);
               }else if (Input.GetButton ("PickTopP1")) {
                    PickItem (other.gameObject, 1);
               }
          }

          if (other.tag.Equals ("Player2")) {
               if (Input.GetButton ("PickBottomP2")) {
                    PickItem (other.gameObject, 0);
               }else if (Input.GetButton ("PickTopP2")) {
                    PickItem (other.gameObject, 1);
               }
          }
     }

     void PickItem(GameObject player,  int index){
          if (player.GetComponent<PlayerControl> ().currWeight < player.GetComponent<PlayerControl> ().maxWeight) {
               if (index == 0 && !bottomPicked) {
                    bottomPicked = true;
                    player.GetComponent<PlayerControl> ().currWeight += bottomItem.GetComponent<Item> ().weight;
                    if (player.GetComponent<PlayerControl> ().currWeight > player.GetComponent<PlayerControl> ().maxWeight)
                         player.GetComponent<PlayerControl> ().currWeight = player.GetComponent<PlayerControl> ().maxWeight;
                    player.GetComponent<PlayerControl> ().cash += bottomItem.GetComponent<Item> ().price;
                    player.GetComponent<PlayerControl> ().items.Add (bottomItem);
                    bottomItem.SetActive (false);
               } else if (index == 1 && !topPicked) {
                    topPicked = true;
                    player.GetComponent<PlayerControl> ().currWeight += topItem.GetComponent<Item> ().weight;
                    if (player.GetComponent<PlayerControl> ().currWeight > player.GetComponent<PlayerControl> ().maxWeight)
                         player.GetComponent<PlayerControl> ().currWeight = player.GetComponent<PlayerControl> ().maxWeight;
                    player.GetComponent<PlayerControl> ().cash += topItem.GetComponent<Item> ().price;
                    player.GetComponent<PlayerControl> ().items.Add (topItem);
                    topItem.SetActive (false);
               }
          }
     }

     void GenerateTopItem(){
          topItem = Instantiate(Inventory.Instance.GetItem(topGroup, topPerc));
          topItem.transform.position = topPoint.transform.position;
          topItem.transform.SetParent(GameObject.Find("ItemsHolder").transform);
     }

     void GenerateBottomItem(){          
          bottomItem = Instantiate(Inventory.Instance.GetItem(bottomGroup, bottomPerc));
          bottomItem.transform.position = bottomPoint.transform.position;
          bottomItem.transform.SetParent(GameObject.Find("ItemsHolder").transform);
     }
}
