﻿using System.Collections;

public enum GameStateType{
     InGame,
     Paused,
     GameEnd,
     Waiting,
     GameStart
};