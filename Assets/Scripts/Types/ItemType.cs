﻿using System.Collections;

public enum ItemType{
     none,
     computing,
     appliances,
     cultural,
     sound
};