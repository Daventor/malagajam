﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cashbox : MonoBehaviour {

     public bool player1 = true;
     public List<GameObject> items = new List<GameObject> ();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

     void OnTriggerEnter (Collider other)
     {
          if (player1 && other.tag.Equals ("Player1")) {
               PlayerControl player = other.GetComponent<PlayerControl> ();

               if (player.items.Count > 0) {                    
                    player.canMove = false;
                    foreach (GameObject item in player.items) {
                         items.Add (item);
                    }

                    player.items.RemoveRange(0, player.items.Count);
                    player.currWeight = 0;
                    player.cash = 0;

                    player.EnableMove (2);

               }
          }

          if (!player1 && other.tag.Equals ("Player2")) {
               PlayerControl player = other.GetComponent<PlayerControl> ();

               if (player.items.Count > 0) {                    
                    player.canMove = false;
                    foreach (GameObject item in player.items) {
                         items.Add (item);
                    }

                    player.items.RemoveRange(0, player.items.Count);
                    player.currWeight = 0;
                    player.cash = 0;

                    player.EnableMove (2);

               }
          }
     }
}
