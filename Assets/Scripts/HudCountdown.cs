﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HudCountdown : MonoBehaviour {

     public Text countdownLbl;    

     int state = 3;

	void Start () {
          iTween.ScaleBy(countdownLbl.gameObject, iTween.Hash("x", .005, "y", .005, "easeType", "easeOutSine", "delay", .5, "oncompletetarget", gameObject, "oncomplete", "nextStep", "time", .5));
	}
		
     public void nextStep(){
          if (state > 1) {
               state--;
               countdownLbl.text = state.ToString ();
               countdownLbl.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);

               iTween.ScaleBy (countdownLbl.gameObject, iTween.Hash ("x", .005, "y", .005, "easeType", "easeOutSine", "delay", .5, "oncompletetarget", gameObject, "oncomplete", "nextStep", "time", .5));
          } else {
               gameObject.SetActive (false);
               GameController.Instance.currState = GameStateType.InGame;
          }
     }
}
